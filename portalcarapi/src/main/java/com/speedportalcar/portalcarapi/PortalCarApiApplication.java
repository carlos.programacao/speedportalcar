package com.speedportalcar.portalcarapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PortalCarApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(PortalCarApiApplication.class, args);
	}

}
