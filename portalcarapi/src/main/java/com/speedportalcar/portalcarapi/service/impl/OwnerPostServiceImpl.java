package com.speedportalcar.portalcarapi.service.impl;

import com.speedportalcar.portalcarapi.client.CarPostClient;
import com.speedportalcar.portalcarapi.dto.OwnerPostDTO;
import com.speedportalcar.portalcarapi.service.OwnerPostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class OwnerPostServiceImpl implements OwnerPostService {

    @Autowired
    private CarPostClient carPostClient;

    @Override
    public void createOwnerCar(OwnerPostDTO ownerPostDTO) {
        carPostClient.ownerPostClient(ownerPostDTO);
    }
}
