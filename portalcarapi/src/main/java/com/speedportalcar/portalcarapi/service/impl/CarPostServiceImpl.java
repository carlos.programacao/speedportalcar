package com.speedportalcar.portalcarapi.service.impl;

import com.speedportalcar.portalcarapi.client.CarPostClient;
import com.speedportalcar.portalcarapi.dto.CarPostDTO;
import com.speedportalcar.portalcarapi.service.CarPostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CarPostServiceImpl implements CarPostService {

    @Autowired
    private CarPostClient carPostClient;

    @Override
    public List<CarPostDTO> getCarForSale() {
        return carPostClient.carForSaleClient();
    }

    @Override
    public void changeCarForSale(CarPostDTO carPostDTO, Long id) {
        carPostClient.changeCarForSaleClient(carPostDTO, id);
    }

    @Override
    public void removeCarForSale(Long id) {
        carPostClient.deleteCarForSaleClient(id);
    }
}
