package com.speedportalcar.portalcarapi.service;


import com.speedportalcar.portalcarapi.dto.CarPostDTO;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface CarPostService {

    List<CarPostDTO> getCarForSale();
    void changeCarForSale(CarPostDTO carPostDTO, Long id);
    void removeCarForSale(Long id);
}
