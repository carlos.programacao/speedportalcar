package com.speedportalcar.portalcarapi.service;

import com.speedportalcar.portalcarapi.dto.OwnerPostDTO;
import org.springframework.stereotype.Service;

@Service
public interface OwnerPostService {

    void createOwnerCar(OwnerPostDTO ownerPostDTO);

}
