package com.speedportalcar.portalcarapi.client;

import com.speedportalcar.portalcarapi.dto.CarPostDTO;
import com.speedportalcar.portalcarapi.dto.OwnerPostDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;

@Component
public class CarPostClient {

    private final String USER_STORE_SERVICE_URI = "http://localhost:8080/user";
    private final String POSTS_STORE_SERVICE_URI = "http://localhost:8080/sales";

    @Autowired
    private RestTemplate restTemplate;

    public List<CarPostDTO> carForSaleClient() {
       ResponseEntity<CarPostDTO[]> responseEntity = restTemplate.getForEntity("/cars", CarPostDTO[].class);
       return Arrays.asList(Objects.requireNonNull(responseEntity.getBody()));
    }

    public void ownerPostClient(OwnerPostDTO ownerPostDTO) {
        restTemplate.postForEntity(USER_STORE_SERVICE_URI, ownerPostDTO, OwnerPostDTO.class);
    }

    public void changeCarForSaleClient(CarPostDTO carPostDTO, Long id) {
        restTemplate.put(POSTS_STORE_SERVICE_URI + "/car/" + id, carPostDTO, CarPostDTO.class);
    }

    public void deleteCarForSaleClient(Long id) {
        restTemplate.delete(POSTS_STORE_SERVICE_URI + "/car/" + id);
    }

}
