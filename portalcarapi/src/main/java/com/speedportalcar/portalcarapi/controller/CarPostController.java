package com.speedportalcar.portalcarapi.controller;

import com.speedportalcar.portalcarapi.dto.CarPostDTO;
import com.speedportalcar.portalcarapi.message.KafkaProducerMessage;
import com.speedportalcar.portalcarapi.service.CarPostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/car")
public class CarPostController {

    @Autowired
    private CarPostService carPostService;

    @Autowired
    private KafkaProducerMessage kafkaProducerMessage;

    @PostMapping("/post")
    public ResponseEntity postCarForSale(@RequestBody CarPostDTO carPostDTO) {
        kafkaProducerMessage.sendMessage(carPostDTO);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @GetMapping("/posts")
    public ResponseEntity<List<CarPostDTO>> getCarSale() {
        List<CarPostDTO> carPostDTO = carPostService.getCarForSale();
        return ResponseEntity.status(HttpStatus.FOUND).body(carPostDTO);
    }

    @PutMapping("/{id}")
    public ResponseEntity<CarPostDTO> changeCarForSale(@RequestBody CarPostDTO carPostDTO, @PathVariable Long id) {
        carPostService.changeCarForSale(carPostDTO, id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity deleteCarForSale(@PathVariable Long id) {
        carPostService.removeCarForSale(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

}
