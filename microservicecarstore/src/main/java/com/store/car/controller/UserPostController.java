package com.store.car.controller;

import com.store.car.dto.UserPostDTO;
import com.store.car.service.UserPostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/user")
public class UserPostController {

    @Autowired
    private UserPostService userPostService;

    @PostMapping
    public ResponseEntity createOwner(UserPostDTO userPostDTO) {
        userPostService.createOwnerPost(userPostDTO);
        return new ResponseEntity<>(HttpStatus.OK);
    }

}
