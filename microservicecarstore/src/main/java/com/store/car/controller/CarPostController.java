package com.store.car.controller;

import com.store.car.dto.CarPostDTO;
import com.store.car.service.CarPostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/sales")
public class CarPostController {

    @Autowired
    private CarPostService carPostService;

    @GetMapping("/cars")
    public ResponseEntity<List<CarPostDTO>> getCarSale() {
        return ResponseEntity.status(HttpStatus.FOUND).body(carPostService.getCarSale());
    }

    @PutMapping("/cars/{id}")
    public ResponseEntity changeCarSale(@RequestBody CarPostDTO carPostDTO, @PathVariable("id") Long id) {
        carPostService.changeCarSale(carPostDTO, id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @DeleteMapping("/car/{id}")
    public ResponseEntity removerCarSale(@PathVariable("id") Long id) {
        carPostService.removeCarSale(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

}
