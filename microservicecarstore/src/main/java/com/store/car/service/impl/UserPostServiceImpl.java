package com.store.car.service.impl;

import com.store.car.dto.UserPostDTO;
import com.store.car.model.UserPost;
import com.store.car.repository.UserPostRepository;
import com.store.car.service.UserPostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserPostServiceImpl implements UserPostService {

    @Autowired
    private UserPostRepository userPostRepository;

    @Override
    public void createOwnerPost(UserPostDTO userPostDTO) {
        UserPost userPost = new UserPost();
        userPost.setName(userPostDTO.getName());
        userPost.setType(userPostDTO.getType());
        userPost.setEmail(userPostDTO.getEmail());
        userPost.setPhone(userPostDTO.getPhone());

        userPostRepository.save(userPost);
    }
}
