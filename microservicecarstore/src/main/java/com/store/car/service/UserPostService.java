package com.store.car.service;

import com.store.car.dto.UserPostDTO;
import org.springframework.stereotype.Service;

@Service
public interface UserPostService {

    void createOwnerPost(UserPostDTO userPostDTO);
}
