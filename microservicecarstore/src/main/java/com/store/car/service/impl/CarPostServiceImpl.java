package com.store.car.service.impl;

import com.store.car.dto.CarPostDTO;
import com.store.car.model.CarPost;
import com.store.car.repository.CarPostRepository;
import com.store.car.repository.UserPostRepository;
import com.store.car.service.CarPostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.NoSuchElementException;

@Service
public class CarPostServiceImpl implements CarPostService {

    @Autowired
    private CarPostRepository carPostRepository;

    @Autowired
    private UserPostRepository userPostRepository;

    @Override
    public void newPostDetails(CarPostDTO carPostDTO) {
        CarPost carPost = mapCarDtoToEntity(carPostDTO);
        carPostRepository.save(carPost);
    }

    @Override
    public List<CarPostDTO> getCarSale() {
        List<CarPostDTO> carSales = new ArrayList<>();
        carPostRepository.findAll().forEach(item -> carSales.add(mapCarEntityToDTO(item)));
        return carSales;
    }

    @Override
    public void changeCarSale(CarPostDTO carPostDTO, Long id) {
        carPostRepository.findById(id).ifPresentOrElse(item -> {
            item.setModel(carPostDTO.getModel());
            item.setBrand(carPostDTO.getBrand());
            item.setPrice(carPostDTO.getPrice());
            item.setDescription(carPostDTO.getDescription());
            item.setEngineVersion(carPostDTO.getEngineVersion());
            item.setCity(carPostDTO.getCity());
            item.setCreatedDate(carPostDTO.getCreatedDate());
            item.setOwnerId(carPostDTO.getOwnerId());
            item.setOwnerType(carPostDTO.getOwnerType());
            item.setContact(carPostDTO.getContact());
            carPostRepository.save(item);
        }, () -> {
            throw new NoSuchElementException();
        });
    }

    @Override
    public void removeCarSale(Long id) {
        carPostRepository.deleteById(id);
    }

    private CarPostDTO mapCarEntityToDTO(CarPost carPost) {
        return CarPostDTO.builder()
                .model(carPost.getModel())
                .brand(carPost.getBrand())
                .price(carPost.getPrice())
                .description(carPost.getDescription())
                .engineVersion(carPost.getEngineVersion())
                .city(carPost.getCity())
                .createdDate(carPost.getCreatedDate())
                .ownerId(carPost.getOwnerId())
                .ownerType(carPost.getOwnerType())
                .contact(carPost.getContact())
                .build();
    }


    private CarPost mapCarDtoToEntity(CarPostDTO carPostDTO) {
        CarPost carPost = new CarPost();

        userPostRepository.findById(carPostDTO.getOwnerId()).ifPresentOrElse(item -> {
            carPost.setUserPost(item);
            carPost.setContact(item.getPhone());
        }, () -> {
          throw new RuntimeException();
        });

        carPost.setModel(carPostDTO.getModel());
        carPost.setBrand(carPostDTO.getBrand());
        carPost.setPrice(carPostDTO.getPrice());
        carPost.setDescription(carPostDTO.getDescription());
        carPost.setEngineVersion(carPostDTO.getEngineVersion());
        carPost.setCity(carPostDTO.getCity());
        carPost.setCreatedDate(String.valueOf(new Date()));

        return carPost;
    }

}
